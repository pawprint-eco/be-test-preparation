# Pawprint BE Test Preparation


Requirements
---

**Linux:**
 - Docker
 - Docker Compose
 - Git
 
**Mac:**
 - Docker Desktop for Mac
 - Git
 
**Windows: _(see note below)_**
 - WSL2 enabled and a distro installed
 - Docker Desktop for Windows 
 - Git for Windows

### Log in to GitLab's container registry

To ensure you're able to download the Docker images used in the test please log in to GitLab's container registry after Docker has been installed:

`docker login registry.gitlab.com`

### Note for Windows Users:

WSL2 is now available in W10. You will need to have it installed and enabled to be able to sit this test properly; please see [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) for instructions. Recommended distros to use are [Ubuntu 20.04 LTS](https://www.microsoft.com/en-gb/p/ubuntu-2004-lts/9n6svws3rx71?rtc=1&activetab=pivot:overviewtab) or [openSUSE Leap 15.1](https://www.microsoft.com/en-gb/p/opensuse-leap-15-1/9njfzk00fgkv?rtc=1&activetab=pivot:overviewtab). 

Please also ensure Docker Desktop is using the WSL2 backend as detailed [here](https://docs.docker.com/docker-for-windows/wsl/).
